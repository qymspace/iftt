import WPAPI from 'wpapi';
import moment from 'moment';


const SITE = "https://cdn1.ethmoide.com/wp-json/"
const OPENAI_API_KEY = "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"
var DAY_COUNTER = 0; //start at 0 to start publishing from today
const POSTS_PER_DAY = 2// Used to reduce increment of publish date e.g for 4 posts per day then increment will be 1/4 per iteration
/*
*Change the DATA source here and make sure everywhere it is used makes sense with the format of data. 
*/
import DATA from "./data/dummy.json" assert { type: "json" }

var wp = new WPAPI({
    endpoint: SITE,
    username: 'qymspace',
    password: '8KPs uoWz IAMM fKfK SNid hnE6'
});



// Request methods return Promises.
wp.posts().get()
    .then(function (data) {
        // do something with the returned posts
        console.log("Posts", data.map(post => ({ "Title": post.title.rendered })))
    })
    .catch(function (err) {
        // handle error
        console.error(JSON.stringify(err))
    });
//Use run as async function to make the requests await their previour requests
run()


async function run() {
    for (let item of DATA) {
        try {
            console.log("Current DAY_COUNTER", DAY_COUNTER)
            const res = await createPost({
                // "title" and "content" are the only required properties
                title: item.name,
                content: item.description,
                image: item.image,
                //Expects to receive date as "2 hours"
                date: item.date ? moment().add(item.date.split(" ")[0], item.date.split(" ")[1]).format('YYYY-MM-DD HH:mm:ss') : null,
                categories: item.categories || []

            })
            console.log("Done! ", item.name, "for DATE: ", res.POST_DATE)

            //For every iteration increase the time to publish by an amount. This amount is smaller if there are more than 
            //one posts per day. i.e 1/4 if there are 4 posts per day. 
            //Don't increment if this post had it's own date
            if (!item.date)
                DAY_COUNTER = DAY_COUNTER + (1 / POSTS_PER_DAY)
        }
        catch (err) {
            console.error("Error! Title: ", item.name, JSON.stringify(err))
        }

    }
}

async function createPost(post) {
    return new Promise(async (resolve, reject) => {
        //Upload media  *****No need to upload media as IFTTT doesnt detect it in feed****
        wp.media().file(post.image || "./data/images/surgery.jpg").create({ title: "Post image" }).then(media => {
            //If date is not set in data then make a schedule using a counter else use the set date and don't
            //increment date
            const POST_DATE = post.date ? post.date : new Date(Date.now() + (DAY_COUNTER * 24 * 60 * 60 * 1000))
            console.log("Posting for DATE: ", POST_DATE)
            wp.posts().create({
                title: post.title,
                content: post.content,
                // Post will be created as a draft by default if a specific "status"
                // is not specified
                status: 'future',
                categories: [24].concat(post.categories),
                date: POST_DATE,
                featured_media: media.id
            }).then(function (response) {
                // "response" will hold all properties of your newly-created post,
                // including the unique `id` the post was assigned on creation
                resolve({ response, POST_DATE })
            })
                .catch(err => {
                    reject(err)
                })

        }).catch((err) => {
            reject(err)
        })
    })
}